FROM debian:latest

RUN apt-get update
RUN apt-get install hello -y
RUN apt-get install git -y

CMD ["hello"]